# goodnight.sh - Copyright (c) 2022 Tomáš Pártl, tomaspartl at centrum.cz

## What is this

This script suspends your computer and if the computer
wakes up again even though you don't want it to,
the script suspends it again.

IMPORTANT: Don't use screen lock on suspend or it will get hard
to wake up the computer when you want to.

This is a workaround for computers waking up from suspend for no apparent reason.
When you suspend your computer using this script and the computer wakes up again,
this script waits 10 seconds (the number is configurable) before going to sleep again.
During that time, you can press CANCEL to make it stay awake.
In other words, this script keeps suspending the computer
unless the user intervenes.

## Requirements

This script uses zenity for the graphic user interface.
On Ubuntu, you can install zenity with
sudo apt-get install zenity.
I bet it's just as easy on other distros :)

## Installation

1) Install zenity (see Requirements).
2) Copy this script somewhere, e. g., to your ~/bin directory
3) Create a desktop launcher for it, so that it's always at hand.

## Lincence

goodnight.sh is released under the MIT license.
