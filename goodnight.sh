#!/bin/bash

# This is a workaround for computers waking up from suspend for no apparent reason.
# When you suspend your computer using this script,
# it waits 10 seconds (the number is configurable) before going to sleep again.
# During that time, you can press CANCEL to make it stay awake.
# In other words, this script keeps suspending the computer
# unless the user intervenes.
#
# It uses zenity for the graphic user interface.
# On Ubuntu, you can install zenity with
# sudo apt-get install zenity.
# I bet it's just as easy on other distros :)
#
# Copyright (c) 2022 Tomáš Pártl, tomaspartl at centrum.cz
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Cheers!

SUSPEND_DELAY=1
WAIT_SECONDS=10
LOG_FILE=
LOG=1

CMD_NAME=`echo "$0" | sed s-^.*/--` 

function log()
{
  if test "$LOG" == "1"
  then
    if test "$LOG_FILE" == ""
    then
      echo "$1"
    else
      echo "$1" >>"${LOG_FILE}"
    fi
  fi
}

function clearlog()
{
  if test "$LOG" == "1"
  then
    if test "$LOG_FILE" != ""
    then
      rm -f "${LOG_FILE}"
    fi
  fi
}

#--------------args ----------------

while test $# -ne 0;
do
	case $1 in
	"-d") SUSPEND_DELAY=$2; shift 2;;
	"-w") WAIT_SECONDS=$2; shift 2;;
	"-l") LOG=$2; shift 2;;
	"-f") LOG_FILE=$2; shift 2;;
	"-s") LOG_FILE=""; LOG=1; shift 2;;
	"-h") echo $'\n'\
=====================================================================================$'\n'\
"$CMD_NAME" - put computer to sleep and make sure it does not wake up prematurely. $'\n'\
===================================================================================== $'\n'\
Usage: "$CMD_NAME" \<options\>$'\n'\
-d \<delay before suspend in seconds.\> Defaults to 1.$'\n'\
-w \<wait this many seconds before going to sleep again.\> Defaults to 5.$'\n'\
-l \<0 or 1.\> Turns logging on or off. 1 means on, 0 means off.$'\n'\
-f \<file path\> Sets the log file. Defaults to stdout.$'\n'\
-s Log to stdout.$'\n'\
Enjoy!
exit 0;;
	esac
done;

zenity --question --text="Wanna put the computer to sleep?"
RES=$?

#echo $RES

if test "$RES" == "1"
then
  exit 0
fi

clearlog

NOW=`date`
log "${NOW}: Going to sleep."

while true; do

  systemctl suspend
  sleep $SUSPEND_DELAY
  
  NOW=`date`
  log "${NOW}: Woke up. Let's wait $WAIT_SECONDS seconds before going to sleep again."

  (
  CNT=0
  while [[ $CNT != $WAIT_SECONDS ]]; do
   CNT=$(($CNT + 1))
   echo $CNT
   echo $((100 * $CNT / $WAIT_SECONDS))
   sleep 1
  done
  ) | zenity --progress --auto-close --text="Press CANCEL or I'm going to sleep again!" --percentage=0
  
  RES=$?
  
  #echo $RES
  
  if [ "$RES" = "1" ]
  then
    NOW=`date`
    log "${NOW}: The user decided to wake the computer up."
    exit 0
  fi

  NOW=`date`
  log "${NOW}: Going back to sleep."

done
